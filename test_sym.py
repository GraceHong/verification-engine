# The MIT License (MIT)
# Copyright (c) 2016 Arie Gurfinkel

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import unittest
import wlang.ast as ast
import wlang.sym

class TestSym (unittest.TestCase):
    def test_one (self):
        prg1 = "havoc x; assume x > 10; assert x > 15"
        ast1 = ast.parse_string (prg1)
        sym = wlang.sym.SymExec ()
        st = wlang.sym.SymState ()
        out = [s for s in sym.run (ast1, st)]
        self.assertEquals (len(out), 1)

    def test_inv_loop (self):
        prg2 = "havoc x, y; assume y >= 0; c := 0; r := x; while c < y inv c <= y and r = x + c do { r := r + 1; c := c + 1}; assert r = x + y"
        ast2 = ast.parse_string (prg2)
        sym = wlang.sym.SymExec ()
        st = wlang.sym.SymState ()
        out = [s for s in sym.run (ast2, st)]
        self.assertEquals (len(out), 1)

    def test_inv_loop_wrong1 (self):  
        #there is error in assert and symbolic is 0,which indicates the inv is not enought 
        prg3 = "havoc x, y; assume y >= 0; c := 0; r := x; while c < y inv c < y and r = x + c do { r := r + 1; c := c + 1}; assert r = x + y"
        ast3 = ast.parse_string (prg3)
        sym = wlang.sym.SymExec ()
        st = wlang.sym.SymState ()
        out = [s for s in sym.run (ast3, st)]
        self.assertEquals (len(out), 0)

    def test_inv_loop_wrong2 (self):
        #there is error in assert which indicates the inv is not enought
        prg3 = "havoc x, y; assume y >= 0; c := 0; r := x; while c < y inv c <= y do { r := r + 1; c := c + 1}; assert r = x + y"
        ast3 = ast.parse_string (prg3)
        sym = wlang.sym.SymExec ()
        st = wlang.sym.SymState ()
        out = [s for s in sym.run (ast3, st)]
        self.assertEquals (len(out), 1)

    def test_inv_loop_wrong3 (self):  
        #y>0 there is no possible for c = y = 0, so if inv:c<y and r = x+c, it's impossible
        prg3 = "havoc x, y; assume y > 0; c := 0; r := x; while c < y inv c < y and r = x + c do { r := r + 1; c := c + 1}; assert r = x + y"
        ast3 = ast.parse_string (prg3)
        sym = wlang.sym.SymExec ()
        st = wlang.sym.SymState ()
        out = [s for s in sym.run (ast3, st)]
        self.assertEquals (len(out), 0)

    def test_inv_loop_wrong4 (self):
        #y>0 inv:missing r= x + c 
        prg3 = "havoc x, y; assume y > 0; c := 0; r := x; while c < y inv c <= y do { r := r + 1; c := c + 1}; assert r = x + y"
        ast3 = ast.parse_string (prg3)
        sym = wlang.sym.SymExec ()
        st = wlang.sym.SymState ()
        out = [s for s in sym.run (ast3, st)]
        self.assertEquals (len(out), 0)

    def test_inv_loop_wrong5 (self):
        #change assume y to negative
        prg3 = "havoc x, y; assume y < 0; c := 0; r := x; while c > y inv c <= y and r = x + c do { r := r + 1; c := c + 1}; assert r = x + y"
        ast3 = ast.parse_string (prg3)
        sym = wlang.sym.SymExec ()
        st = wlang.sym.SymState ()
        out = [s for s in sym.run (ast3, st)]
        self.assertEquals (len(out), 0)

    def test_noinv_loop (self):
        prg1 = "havoc x, y; assume y >= 0; c := 0; r := x; while c < y do { r := r + 1; c := c + 1}; assert r = x + y"
        ast1 = ast.parse_string (prg1)
        sym = wlang.sym.SymExec ()
        st = wlang.sym.SymState ()
        out = [s for s in sym.run (ast1, st)]
        self.assertEquals (len(out), 11)

    def test_1_loop (self): 
		# simple loop
		prg1 = "x:=5; while x >=0 do x := x - 1"
		ast1 = ast.parse_string (prg1)
		sym = wlang.sym.SymExec ()
		st = wlang.sym.SymState ()
		out = [s for s in sym.run (ast1, st)]
		self.assertEquals(len(out), 1)

    def test_2_loop (self):
		# 3 output states for 3 path (one for X = 3, one for X = 4 and one for any X > 5)
		prg1 = "havoc x; assume x > 2; while x < 5 do x := x + 1; skip"
		ast1 = ast.parse_string (prg1)
		sym = wlang.sym.SymExec ()
		st = wlang.sym.SymState ()
		out = [s for s in sym.run (ast1, st)]
		self.assertEquals(len(out), 3)

    def test_3_loop (self):
		# 11 output states, while loop hits max_iter limit
		prg1 = "havoc z; while z >= 0 do z := z - 1"
		ast1 = ast.parse_string (prg1)
		sym = wlang.sym.SymExec ()
		st = wlang.sym.SymState ()
		out = [s for s in sym.run (ast1, st)]
		self.assertEquals(len(out), 11)

    def test_full_loop (self):
    	prg1 = "i := 0;while i < 5 do{  i := i + 1;  print_state;  j := 0;  while j < 3 do  { j := j + 1 };  j := 0};print_state;assert i = 0"
    	ast1 = ast.parse_string (prg1)
    	sym = wlang.sym.SymExec ()
    	st = wlang.sym.SymState ()
    	out = [s for s in sym.run (ast1, st)]
    	print out
    	self.assertEquals (len(out), 0)


    def test_1_if (self):      
		# Nested If, 3 output states
		prg1 = "havoc x; havoc y; if not (x = 1) then {if y <= 2 then x:= 5 else x:=-5} else x := 10"
		ast1 = ast.parse_string (prg1)
		sym = wlang.sym.SymExec ()
		st = wlang.sym.SymState ()
		out = [s for s in sym.run (ast1, st)]
		self.assertEquals(len(out), 3)

    def test_2_if (self):
    	prg1 = "havoc x; if x < 5 then x := 3"
    	ast1 = ast.parse_string (prg1)
    	sym = wlang.sym.SymExec ()
    	st = wlang.sym.SymState ()
    	out = [s for s in sym.run (ast1, st)]
    	self.assertEquals(len(out), 2)

    def test_3_if (self):
		prg1 = "havoc x; if x > 5 then x:= -1*5 else x:= 8/4"
		ast1 = ast.parse_string (prg1)
		sym = wlang.sym.SymExec ()
		st = wlang.sym.SymState ()
		out = [s for s in sym.run (ast1, st)]
		self.assertEquals (len(out), 2)

    def test_4_if (self):
		prg1 = "havoc x;havoc y; if x > 5 or y > 3 then x:= -1*5 else x:= 8/4"
		ast1 = ast.parse_string (prg1)
		sym = wlang.sym.SymExec ()
		st = wlang.sym.SymState ()
		out = [s for s in sym.run (ast1, st)]
		self.assertEquals (len(out), 2)

    def test_full_if (self):
		prg1 = "x := 0; y := 0; z := 0;havoc a;if a > 0 then x := -2 ;havoc b;havoc c;if b < 5 then{if a <= 0 and c > 0 then y := 1;z := 2};assert (not (x + y + z = 3))"
		ast1 = ast.parse_string (prg1)
		sym = wlang.sym.SymExec ()
		st = wlang.sym.SymState ()
		out = [s for s in sym.run (ast1, st)]
		self.assertEquals (len(out), 4)

    def test_basic1 (self):
		#simple test
		prg1 = "havoc x ; y := x"
		ast1 = ast.parse_string (prg1)
		sym = wlang.sym.SymExec ()
		st = wlang.sym.SymState ()
		out = [s for s in sym.run (ast1, st)]
		self.assertEquals (len(out), 1)

    def test_basic2 (self):
		#simple test
		prg1 = "havoc x;assume x > 0"
		ast1 = ast.parse_string (prg1)
		sym = wlang.sym.SymExec ()
		st = wlang.sym.SymState ()
		out = [s for s in sym.run (ast1, st)]
		self.assertEquals (len(out), 1)
		
    def test_basic3 (self):
		#simple test
		prg1 = "havoc x ;assert x > 0"
		ast1 = ast.parse_string (prg1)
		sym = wlang.sym.SymExec ()
		st = wlang.sym.SymState ()
		out = [s for s in sym.run (ast1, st)]
		self.assertEquals (len(out), 1)

    #def test_main(self):
    	#non-functional test 
    	#wlang.sym.main() 

    def test_non_state(self):
    	prg1 = "skip"
    	ast1 = ast.parse_string (prg1)
    	sym = wlang.sym.SymExec ()
    	st = wlang.sym.SymState ()
    	out = [s for s in sym.run (ast1, st)]
    	self.assertEquals (len(out), 1)

    def test_q4 (self):
        prg2 = "havoc x, y, z; assume y >= 0;assume z >= 0; a := 0; c := 0; r := x; while c < y inv c <= y and r = x + z*c do {while a < z inv a <= z and r = x + a do{r := r + 1; a := a + 1}; c := c + 1}; assert r = x + z*y "
        ast2 = ast.parse_string (prg2)
        sym = wlang.sym.SymExec ()
        st = wlang.sym.SymState ()
        out = [s for s in sym.run (ast2, st)]
        self.assertEquals (len(out), 1)







  
	


      
	