havoc x, y, z; 
assume y >= 0;
assume z >= 0;
a := 0; 
c := 0; 
r := x;
while c < y 
inv c <= y and r = x + z*c 
do 
{
while a < z
inv a <= z and r = x + a
do
{
r := r + 1; 
a := a + 1	
};
c := c + 1
}; 
assert r = x + z*y;
print_state
